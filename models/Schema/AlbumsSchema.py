from marshmallow import Schema


class AlbumsSchema(Schema):
    class Meta:
        fields = ("alb_id", "alb_art", "alb_nom", "alb_annee", "alb_prix")
