from datetime import time


class HaveItems:
    id_items: int
    id_user: int
    time: time

    def __init__(self, id_items, id_user, time):
        self.id_items = id_items
        self.id_user = id_user
        self.time = time

    def __repr__(self):
        return (
            f"**HaveItems**"
            f"id_items: {self.id_items} "
            f"id_user: {self.id_user} "
            f"time: {self.time} "
            f"**HaveItems**"
        )
