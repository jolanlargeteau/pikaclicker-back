class Item():
    """
    Classe Items
    """

    id: int
    name: str
    price: int
    gain: int

    def __init__(self, id, name, price, gain):
        self.id = id
        self.name = name
        self.price = price
        self.gain = gain

    def __repr__(self):
        return (
            f"**Items**"
            f"id: {self.id} "
            f"name: {self.name} "
            f"price: {self.price} "
            f"gain: {self.gain} "
            f"**Items**"
        )