from datetime import time


class Bonustrigger:
    id_bonus: int
    id_user: int
    time: time

    def __init__(self, id_bonus, id_user, time):
        self.id_bonus = id_bonus
        self.id_user = id_user
        self.time = time

    def __repr__(self):
        return (
            f"**BonusTrigger**"
            f"id_bonus: {self.id_bonus} "
            f"id_user: {self.id_user} "
            f"time: {self.time} "
            f"**BonusTrigger**"
        )
