from datetime import time


class User:
    """
    Classe User
    """
    id: int
    name: str
    password: chr(60)
    points = int
    scores = float
    last_items_trigger: time

    def __init__(self, id, name, password, points, scores, last_items_trigger):
        self.id = id
        self.name = name
        self.password = password
        self.points = points
        self.scores = scores
        self.last_items_trigger = last_items_trigger

    def __repr__(self):
        return (
            f"**User**"
            f"id: {self.id} "
            f"name: {self.name} "
            f"password: {self.password} "
            f"points: {self.points} "
            f"scores: {self.scores} "
            f"last_items_trigger: {self.last_items_trigger} "
            f"**User**"
        )
