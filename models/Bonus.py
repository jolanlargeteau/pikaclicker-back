class Bonus():
    """
    Classe Bonus
    """
    id: int
    name: str
    image: str
    gain: int
    respawn_range: int

    def __init__(self, id, name, image, gain, respawn_range):
        self.id = id
        self.name = name
        self.image = image
        self.gain = gain
        self.respawn_range = respawn_range

    def __repr__(self):
        return (
            f"**Bonus**"
            f"id: {self.id} "
            f"name: {self.name} "
            f"image: {self.image} "
            f"gain: {self.gain} "
            f"respawn_range: {self.respawn_range} "
            f"**Bonus**"
        )