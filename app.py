from flask import Flask
from flask_mysql_connector import MySQL

from models.Schema.AlbumsSchema import AlbumsSchema

# App + Database settings
app = Flask(__name__)
app.config['MYSQL_USER'] = 'root'
app.config['MYSQL_PASSWORD'] = ''
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_DATABASE'] = 'pika_cliker'
mysql = MySQL(app)

# Définition des routes
@app.route('/')
def hello_world():
    #full_schema = AlbumsSchema()
    #results = full_schema.dump(arr)
    #print(results)

    return 'Hello World'

if __name__ == '__main__':
    app.run(debug=True)
