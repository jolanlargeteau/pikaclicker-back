# PikaClicker-back

## Présentation
Bonjour, bienvenue sur notre IdleGame *pikaclicker*.  
Ceci est un jeu fait par des fans, est n'est en aucun cas relié à nintendo.  
Listes des participants :
| Nom | Prénom |
|-----|------|
|LARGETEAU|Jolan|
|VELOSO-PAULOS|Ruben|

## Technique
Application codé en Python utilisant le micro framework Flask.

## Installation
1. Cloner le code  
2. Créer le virtual env : `py -3 -m venv venv`  
3. Installer les requirements : `pip install -r requirements.txt`  
4. Création des vars d'env : `set FLASK_APP=app.py`  
5. Lancement de l'app : `flask run`
